# Haskell_HTTP_Library 
Das folgende Projekt wurde von Joshua Fett, Jens Niederreiter und Jan Kusnierz im Rahmen der Veranstaltung CS2333 Funktionale Programmierung
im Sommersemester 2019 entwickelt.

## Mitglieder:
* Joshua Fett - 5115534
* Jens Niederreiter - 5159419
* Jan Kusnierz - 5112294

## Inspiration:
Im Alltag und auf der Arbeit verwenden wir gerne das Javascript-Framework Node, mit der Erweiterung Express. Dieses Framework ermöglicht es Backend-Applikationen mit Hilfe von Javascript aufzusetzen und serverseitig laufen zu lassen. Hierbei wird RESTfulness groß geschrieben und ermöglicht das schnelle und einfache Aufsetzen von modernen APIs.
<br />
<br />
Der grundsätzliche Aufbau für den Umgang mit HTTP-Requests ist es einen Eventlistener zu registrieren, welcher auf eine Route lauscht und sobald eine Anfrage reingeht, diese mit einem Callback verarbeitet.
Die unterschiedlichen HTTP-Methoden werden dabei durch eigene Methoden für das Lauschen realisiert.

Das Vorbild mit Express:

```javascript
var ex = require("express");
var express = ex();

function getRootHandler(res,req){
    if(req.headers["content-type"] == "application/json"){
        res.status(200);
        res.set({"Content-Type": "json"});
        res.send("{\"Body\": \"das ist meine Antwort\"}");
    }else{
        res.status(415);
        res.end();
    }
}

express.get("/", getRootHandler);

express.listen(3000, () => console.log("Server started on 3000"));
```
Der Gedanke eines Callbacks, welcher eine Anfrage reinbekommt und eine Antwort rausgibt, hat uns sehr gut gefallen!
<br />

Unsere Implementierung:
```javascript
import Server

getRootHandler req = if((get "Content-Type" req) == Just "application/json")
    then Response {
        responseHeader = [
            StatusLine 200 "OK",
            ContentType "application/json"
        ],
        responseBody = Just "{\"Body\": \"das ist meine Antwort\"}"
    }
    else _415

listenOn "localhost" 3000 [
        GET "/" getRootHandler
    ]
```

## Aufbau:
Die wichtigsten Komponenten unserer Bibliothek sind die folgenden Module:
1. Server.hs : Dieses Modul kommuniziert mit dem Netzwerk-Socket und reicht Anfragen und Antworten weiter. 
2. Callback.hs: Hier befindet sich der eigentliche Handler für die Verarbeitung von Anfragen. Ebenso sind hier Funktionen für das Auffinden des richtigen Handlers, sowohl für HTTP-Requests, als auch für das Routen.
3. Request.hs: Dieses Modul ist für das Erstellen von Request-Typen zuständig. Das eigentliche HTTP-Request kommt als ByteString rein und muss zu einem Typen geparsed werden, mit dem wir später weiterarbeiten können.
4. Response.hs: Hier können wir eine Antwort zusammensetzen, welche wir als ByteString über den Netzwerk-Socket an den Client senden können.
5. DefaultResponses.hs: Viele Antworten benötigen keinen Body, da der Status-Code in vielen Fällen ausreichend ist. Dieses Modul beinhaltet einige Antworten, die nur einen Status-Code beinhalten.
6. ~~Payload.hs: Request und Response-Body können in unterschiedlicher Form auftreten. Wir unterstützen XML, JSON und Text.~~
Verarbeitung vom Body wird dem User überlassen. Wir empfehlen Aeson für Json.
7. Cookie.hs: Damit können wir Zustände innerhalb einer Session darstellen.

## Umfang:
* RouteParameter: URL-Abschnitte welche als Ressource ID fungieren
* ~~Bearer Tokens: Support für JWT (JSON Web Token)~~ Nächste Iteration!
* Cookies
* Routing
* CRUD-Operationen (POST, GET, PUT, DELETE)
* Ebenso PATCH, DELETE, OPTIONS, HEAD
* ~~HATEOAS Support~~


## Besonderheiten:
* Unsupported Method-Handling: Wenn ein Pfad definiert ist, aber der Request keine gültige Operation beinhaltet (HTTP-Verb), kann man explizit ein _405 Method Not Allowed_ Status zurückgeben. Dies wird durch __CATCH__ realisiert. Ist CATCH nicht gesetzt wird mit dem _500 Internal Server Error_ Status geantwortet.
* Routing: Routing realisieren wir durch die zwei Konstruktoren __ROUTE_FILE__ und __ROUTE_DIR__. Diese erwarten als ersten Parameter die Route unter welcher man die Datei/das Verzeichnis auffinden soll, der zweite ist der Standort innerhalb des Servers.
* Default Responses: Für Antworten, welche nur einen Status-Code beinhalten empfehlen wir unsere Default Responses. Derzeit unterstützen wir nicht alle Status-Codes, aber die meisten der 2xx, 4xx und 5xx Statusgruppen. Eine DefaultResponse beginnt mit einem Unterstrich gefolgt vom Status-Code. ZB: __\_405__


## Anmerkungen:
Wir empfehlen ein Linux Derivat als Plattform für den Server. Andere UNIX-artige Betriebssysteme wie MacOS könnten auch noch Funktionieren.
Lediglich gewährleisten wir keinerlei Erfolg mit Mircrosoft Windows, da die Kommunikation mit dem Netzwerk-Socket für UNIX ausgelegt ist.
<br />
<br />
Dezeit unterstützen wir nur die Statusgruppen 2xx, 4xx und 5xx.<br />

Für weitere Informationen besuche doch unsere Dokumentation.

## Entwickeln:
Importiere das Server Module und fang mit dem Coden an! Ausgeführt wird die Applikation durch das Aufrufen der Datei mit ghci und dem starten der dafür definierten Methode. Schau dir doch das Beispiel __Playground.hs__ an, lade sie mit ghci Playground.hs und starte sie mit dem Methodenaufruf __main__.