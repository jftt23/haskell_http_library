{-|
Module      : Server
Description : Creates the connection via network socket and handles communication.
License     : BSD 3-clause aka New BSD
Maintainer  : joshua.fett@mni.thm.de
Stability   : Draft 

This module bundles every needed module within the library. It also creates the main communication between our library and
the network socket. 
-}
module Server (module Callback,
    module Cookie,
    module Response,
    module DefaultResponse,
    module Request,
    module Server) where
    
    import Payload
    import Callback
    import Cookie
    import Response
    import DefaultResponse
    import Request
   
    import System.Directory (doesFileExist, doesDirectoryExist)
    import Network.Socket as Ns
    import Network.Socket.ByteString as Bs
    import qualified Data.ByteString.Char8 as C
    import Control.Concurrent
    import Control.Concurrent.MVar
    import System.IO
    import Data.Maybe
    import Data.List

    
    -- | This is our main function for running backend applications. 
    -- It creates the communication between client and server and handles every incoming and outgoing message.
    -- The first parameter is the domain, the second the port and the third is the list of RequestHandlers. 
    listenOn :: String 
        -> Int 
        -> [RequestHandler]
        -> IO ()
    listenOn host port requestHandlers = withSocketsDo $ do
        let hints = defaultHints { 
            addrSocketType = Stream, 
            addrFlags = [AI_PASSIVE]   
        }

        address:_ <- getAddrInfo (Just hints) (Just host) $ Just (show port)
        connection <- socket (addrFamily address) (addrSocketType address) defaultProtocol

        bind connection (addrAddress address)
        listen connection 5

        processLock <- newMVar ()
        processRequests processLock connection

        where
            processRequests :: MVar () 
                -> Socket 
                -> IO ()
            processRequests lock socket = do 
                (connectionSocket, clientAddress) <- accept socket
                print $ "Incoming Request by: " ++ show clientAddress
                forkIO $ processMessages lock connectionSocket
                processRequests lock socket
                
            processMessages :: MVar () 
                -> Socket 
                -> IO ()
            processMessages lock connsock = do 
                connectionHandler <- socketToHandle connsock ReadMode
                hSetBuffering connectionHandler LineBuffering
                messages <- hGetContents connectionHandler
                
                let originalRequest = createRequest messages
                let originalTarget = getRequestTarget (requestHeader originalRequest)
                
                if(isJust $ find(=='.') $ snd' originalTarget)
                    then
                        let filePath = findFile (snd' originalTarget) requestHandlers  
                        in
                            if(isJust filePath)
                                then do
                                    isFile <- doesFileExist $ fromJust filePath                                    
                                    if(isFile)
                                        then do
                                            file <- readFile $ fromJust filePath
                                            sendAll connsock 
                                                $ C.pack
                                                $ responseToString Response {
                                                    responseHeader = [StatusLine 200 "Ok"],
                                                    responseBody = Just file
                                                }
                                    else
                                        sendAll connsock 
                                            $ C.pack
                                            $responseToString _404
                            else
                                sendAll connsock 
                                    $ C.pack 
                                    $ responseToString _404
                else do
                    let newReqTar = paramRouteChecker originalRequest originalTarget requestHandlers
                    let modifiedRequest = if isJust newReqTar then fst (fromJust newReqTar) else originalRequest
                    print modifiedRequest
                    let modifiedTarget = if isJust newReqTar then snd (fromJust newReqTar) else originalTarget
                    print modifiedTarget
                    
                    sendAll connsock 
                        $ C.pack 
                        $ responseToString 
                        $ (findHandler (fst' modifiedTarget) (snd' modifiedTarget) requestHandlers) modifiedRequest

                hClose connectionHandler
                