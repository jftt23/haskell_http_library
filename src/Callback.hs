{-|
Module      : Callback
Description : Defines the structure for request handlers
License     : BSD 3-clause aka New BSD
Maintainer  : joshua.fett@mni.thm.de
Stability   : Draft    

This module contains the main aspect of handling with requests and responses. 
It is similiar with callback methods within frameworks like express or anonymous classes/lambda functions
for eventlistener like click events in JavaFX.
-}
module Callback where
    import Request
    import Response
    import DefaultResponse
    import Data.Maybe
    import Data.List
    import Data.List.Split


    data RequestHandler = 
        GET String (Request -> Response) -- ^ Constructor for GET request handling.
        | POST String (Request -> Response) -- ^ Constructor for POST request handling.
        | PUT String (Request -> Response) -- ^ Constructor for PUT request handling.
        | PATCH String (Request -> Response) -- ^ Constructor for PATCH request handling.
        | DELETE String (Request -> Response) -- ^ Constructor for DELETE request handling.
        | OPTIONS String (Request -> Response) -- ^ Constructor for OPTIONS request handling.
        | HEAD String (Request -> Response) -- ^ Constructor for HEAD request handling.
        | CATCH String (Request -> Response) -- ^ Constructor for catching undefined http methods for defined paths.
        | ROUTE_FILE String String -- ^ Constructor for file routing. 
        -- The first parameter is the path under where the ressource can be reached, the second is the ressource's location.
        | ROUTE_DIR String String -- ^ Constructor for directory routing. 
        -- The first parameter is the path under where the ressources can be reached, the second is the ressource's location.
        

    -- | Helper function for extracting the request's type of a RequestHandler. 
    getRequestType :: RequestHandler -> String
    getRequestType (GET _ _)     = "GET"
    getRequestType (POST _ _)    = "POST"
    getRequestType (PUT _ _)     = "PUT"
    getRequestType (PATCH _ _)   = "PATCH"
    getRequestType (DELETE _ _)  = "DELETE"
    getRequestType (OPTIONS _ _) = "OPTIONS"
    getRequestType (HEAD _ _)    = "HEAD"
    getRequestType (CATCH _ _)   = "CATCH"
    getRequestType (ROUTE_FILE _ _) = "FILE"
    getRequestType (ROUTE_DIR _ _)  = "DIRECTORY"

    -- |'getPath' takes a RequestHandler and returns its Route.
    getPath 
        :: RequestHandler -- ^ RequestHandler
        -> String -- ^ Request Route
    getPath (GET path _)     = path
    getPath (POST path _)    = path
    getPath (PUT path _)     = path
    getPath (PATCH path _)   = path
    getPath (DELETE path _)  = path
    getPath (OPTIONS path _) = path
    getPath (HEAD path _)    = path
    getPath (CATCH path _)   = path
    getPath (ROUTE_FILE path _) = path
    getPath (ROUTE_DIR path _) = path

    -- |'getHandler' takes a RequestHandler and returns its Handler function.
    -- Request Type of the RequestHandler has to be 
    -- GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD or CATCH
    getHandler 
        :: RequestHandler -- ^ RequestHandler
        -> (Request -> Response) -- ^ Handler function
    getHandler (GET _ callback)     = callback
    getHandler (POST _ callback)    = callback
    getHandler (PUT _ callback)     = callback
    getHandler (PATCH _ callback)   = callback
    getHandler (DELETE _ callback)  = callback
    getHandler (OPTIONS _ callback) = callback
    getHandler (HEAD _ callback)    = callback
    getHandler (CATCH _ callback)   = callback

    -- |'getFilePath' takes a RequestHandler and returns the path of the file
    -- Request Type of the RequestHandler has to be ROUTE_FILE or ROUTE_DIR
    getFilePath 
        :: RequestHandler -- ^ RequestHandler
        -> String -- ^ Path of file
    getFilePath (ROUTE_FILE _ filepath) = filepath
    getFilePath (ROUTE_DIR _ filepath) = filepath

    -- |'isParamRoute' checks if the Request Route and the Route of the RequestHandler match.
    -- If true returns a Tuple with the Parameter and the RequestHandler, 
    -- else returns Nothing
    isParamRoute
        :: String -- ^ Request Route
        -> RequestHandler -- ^ RequestHandler 
        -> Maybe (String, RequestHandler) -- ^ Maybe (Parameter, RequestHandler)
    isParamRoute path handler = do
        let sPath = splitOn "/" path
        let sHandler = splitOn "/" $ getPath handler
        if isPrefixOf ":" $ last sHandler 
            then 
                if (init sPath) == (init sHandler) 
                    then Just (last sPath, handler)
                else Nothing
        else Nothing

    -- |'findParamRoute' checks if the Request Route matches with a Parameter Route 
    -- from the list of RequestHanlders. If true returns a Tuple
    -- with the Parameter and the matching RequestHandler.
    findParamRoute 
        :: String -- ^ Request Route
        -> [RequestHandler] -- ^ List of RequestHandlers
        -> Maybe (String, RequestHandler) -- ^ Maybe (Parameter, RequestHandler)
    findParamRoute path handlers = 
        let paramRoute = filter isJust $ map (\x -> isParamRoute path x) handlers
        in
            if(length paramRoute == 0)
                then
                    Nothing
            else 
                head paramRoute
    
    -- |'paramRouteChecker' checks if the Route of a Request is a Parameter Route.
    -- If true returns a Tuple of the old Request but with the Params as requestVar 
    -- and a new Request Target, where the Request Route has been switched with the
    -- RequestHandler Route (example: /user/123 -> /user/:id), so it can later
    -- be matched with the RequestHandler again.
    paramRouteChecker 
        :: Request -- ^ Request
        -> (String,String,String) -- ^ Request Target
        -> [RequestHandler] -- ^ List of RequestHandlers
        -> Maybe (Request, (String, String, String)) -- ^ Maybe (new Request, new Request Target)
    paramRouteChecker oRequest oTarget requestHandlers = 
        if (length (filter (\x -> (snd' oTarget) == (getPath x)) requestHandlers)) == 0
            then do
                let pathVar = findParamRoute (snd' oTarget) requestHandlers
                if length pathVar > 0 
                    then 
                        Just 
                        (createRequestVar (fst $ fromJust pathVar) oRequest
                        , (cPath (getPath (snd $ fromJust pathVar)) oTarget))
                    else Nothing
                else Nothing


    -- | Finds the handler that matches the RequestTargets attributes. 
    findHandler :: String 
        -> String 
        -> [RequestHandler] 
        -> (Request -> Response)
    findHandler reqType path handlers = 
        let handler = filter(\x -> getPath x == path) handlers
        in 
        if(length handler == 0)
            then (\n -> _404)
        else
            let y = find(\x -> getRequestType x == reqType) handler
            in 
            if(isJust y)
                then getHandler $ fromJust y
            else 
                let z = find(\x -> getRequestType x == "CATCH") handler
                in 
                if(isJust z)
                    then getHandler $ fromJust z
                else
                    (\n -> _500)

    
    -- | Extracts the path of a requested file.
    extractPath :: String -> [Char]
    extractPath path = foldl(\x y -> x ++ "/" ++ y) [] 
        $ init 
        $ splitOn "/" 
        $ tail path


    -- | Finds the file that is requested. 
    findFile :: String 
        -> [RequestHandler] 
        -> Maybe String
    findFile target handlers = 
        let fileResult = find(\x -> target == (getPath x)) $ filter(\y -> getRequestType y == "FILE") handlers
        in
            if(isJust fileResult)
                then Just (getFilePath $ fromJust fileResult)
            else
                let 
                    pathWithoutFile = extractPath target
                    directory = find(\x -> pathWithoutFile == (getPath x)) handlers
                in
                    if(isJust directory)
                        then Just ((getFilePath $ fromJust directory) ++ (last $ splitOn "/" target))
                    else
                        Nothing
