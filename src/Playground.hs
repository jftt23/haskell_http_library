import Server
import Data.Maybe

--------------------------------- Ressources ---------------------------------
-- A handler that demonstrates a standard get request
getRootHandler req = Response {
    responseHeader = [
        StatusLine 200 "OK",
        AccessControlAllowOrigin "*",
        ContentType "application/json"
    ],
    responseBody = Just ("{\"Body\": \"Das ist meine Antwort\"}" )
}

-- A handler that demonstrates a post request and response with the request body
postUserData req = Response {
    responseHeader = [
        StatusLine 201 "Created",
        AccessControlAllowOrigin "*",
        ContentType "application/json"
    ],
    responseBody = requestBody req
}

-- A handler that demonstrates route parameters for GET, PUT, PATCH and DELETE
getIdHandler req = Response {
    responseHeader = [
        StatusLine 200 "OK",
        AccessControlAllowOrigin "*",
        ContentType "application/json"
    ],
    responseBody = Just ("{\"Body\": \"" ++ (getRequestVar req) ++ "\"}" )
}


--------------------------------- Cookies ---------------------------------
-- A handler that demonstrates cookie setting
registerUser req = Response {
    responseHeader = [
        StatusLine 201 "Created",
        ContentType "text/plain",
        SetCookie Cookie {
            cookieName = "secret_key",
            cookieValue = "Pa55w0rd",
            cookieHeader = [
                Path "/",
                Expires "-1"
            ]
        }
    ],
    responseBody = pure (++) <*> pure("Willkommen: ") <*> (requestBody req)
}

-- A handler that demonstrates reading cookie values
getCookies req = Response {
    responseHeader = [
        StatusLine 200 "OK",
        ContentType "application/json"
    ],
    responseBody = Just $ if(isJust $ getCookie "secret_key" req) 
        then "{\"CookieWert\": \"" ++ (cookieValue $ fromJust $ getCookie "secret_key" req) ++ "\"}"
        else "{\"CookieWert\": \"Kein Cookie gesetzt\"}"
}


validateUser req = if(isJust $ getCookie "secret_key" req)
    then 
        if((cookieValue $ fromJust $ getCookie "secret_key" req) == "Pa55w0rd")
            then Response {
                responseHeader = [
                    StatusLine 200 "Ok"
                ],
                responseBody = Just "Willkommen" 
            }
        else _401
    else _400


-- Our main entry point. This is were we define on which host and port we're going to listen and the handlers
main = listenOn "localhost" 3000 [
            GET   "/user" getRootHandler
            , POST  "/user" postUserData
            , CATCH "/user" (\req -> _405)
            , GET   "/var/:id" getIdHandler
            
            , POST  "/cookie" registerUser
            , GET   "/cookie" getCookies
            , GET   "/cookie/user" validateUser
            
            , ROUTE_FILE "/test/readme.md" "../README.md"
            , ROUTE_DIR "/doc" "../docs/"
            , ROUTE_DIR "/test" "../test/"
        ]
