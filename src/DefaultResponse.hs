{-|
Module      : DefaultResponse
Description : Functions for the most typical responses without body
License     : BSD 3-clause aka New BSD
Maintainer  : joshua.fett@mni.thm.de
Stability   : Draft

The following module provides a short way of responding to the client, without the need of defining
unnecessary long responses.
-}
module DefaultResponse where
    import Response
    
    ------------------ 2XX Status Code ------------------
    _200 :: Response -- ^ returns a "Response" with the status line 200 \"OK\"
    _200 = Response {
        responseHeader = [StatusLine 200 "Ok"],
        responseBody = Nothing
    }
    _201 :: Response -- ^ returns a "Response" with the status line 201 \"Created\"
    _201 = Response {
        responseHeader = [StatusLine 201 "Created"],
        responseBody = Nothing
    }
    _202 :: Response -- ^ returns a "Response" with the status line 202 \"Accepted\"
    _202 = Response {
        responseHeader = [StatusLine 202 "Accepted"],
        responseBody = Nothing
    }
    _204 :: Response -- ^ returns a "Response" with the status line 204 \"No Content\"
    _204 = Response {
        responseHeader = [StatusLine 204 "No Content"],
        responseBody = Nothing
    }

    ------------------ 4XX Status Code ------------------
    _400 :: Response -- ^ returns a "Response" with the status line 400 \"Bad Request\"
    _400 = Response {
        responseHeader = [StatusLine 400 "Bad Request"],
        responseBody = Nothing
    }
    _401 :: Response -- ^ returns a "Response" with the status line 401 \"Forbidden\"
    _401 = Response {
        responseHeader = [StatusLine 401 "Forbidden"],
        responseBody = Nothing
    }
    _403 :: Response -- ^ returns a "Response" with the status line 403 \"Unauthorized\"
    _403 = Response {
        responseHeader = [StatusLine 403 "Unauthorized"],
        responseBody = Nothing
    }
    _404 :: Response -- ^ returns a "Response" with the status line 404 \"Not Found\"
    _404 = Response {
        responseHeader = [StatusLine 404 "Not Found"],
        responseBody = Nothing
    }
    _405 :: Response -- ^ returns a "Response" with the status line 405 \"Method Not Allowed\"
    _405 = Response {
        responseHeader = [StatusLine 405 "Method Not Allowed"],
        responseBody = Nothing
    }
    _406 :: Response -- ^ returns a "Response" with the status line 406 \"Not Acceptable\"
    _406 = Response {
        responseHeader = [StatusLine 406 "Not Acceptable"],
        responseBody = Nothing
    }
    _411 :: Response -- ^ returns a "Response" with the status line 411 \"Length Required\"
    _411 = Response {
        responseHeader = [StatusLine 411 "Length Required"],
        responseBody = Nothing
    }
    _415 :: Response -- ^ returns a "Response" with the status line 415 \"Unsupported Media Type\"
    _415 = Response {
        responseHeader = [StatusLine 415 "Unsupported Media Type"],
        responseBody = Nothing
    }
    _418 :: Response -- ^ returns a "Response" with the status line 418 \"I'm A Teapot\"
    _418 = Response {
        responseHeader = [StatusLine 418 "I'm A Teapot"],
        responseBody = Nothing
    }
    _422 :: Response -- ^ returns a "Response" with the status line 422 \"Unprocessable Entity\"
    _422 = Response {
        responseHeader = [StatusLine 422 "Unprocessable Entity"],
        responseBody = Nothing
    }

    ------------------ 5XX Status Code ------------------
    _500 :: Response -- ^ returns a "Response" with the status line 500 \"Internal Error\"
    _500 = Response {
        responseHeader = [StatusLine 500 "Internal Sever Error"],
        responseBody = Nothing
    }