{-|
Module      : Cookie
Description : Realization of http cookies
License     : BSD 3-clause aka New BSD
Maintainer  : joshua.fett@mni.thm.de
Stability   : Draft

This module realizes http cookies and their behavior after the <https://tools.ietf.org/html/rfc6265 RFC-6265>.
-}
module Cookie where
    import Data.Maybe 
    import Data.List
    import Data.Char
    import Data.List.Split

 
    -- | Realizes cookies for \"statefulness\" between requests.
    data Cookie = Cookie {
        cookieName :: String, -- ^ The Cookie's name. Should stick to the standard conventions for declaring variables. 
        cookieValue :: String, -- ^ The Cookie's value. The cookieName in combination with the cookieValue is similar to key-value pairs.
        cookieHeader :: [CookieHeaderAttribute] -- ^ The Cookies's header is a list of CookieHeaderAttributes.
    } deriving (Show,Eq)
    

    -- | Cookie header attribute
    data CookieHeaderAttribute = 
        Path String -- ^ Defines a subset of the domain where the Cookie is valid.
        | Expires String -- ^ Should use the following time format: Wdy, DD-MM-YYYY HH:MM:SS GMT.
        | HttpOnly Bool -- ^ The HttpOnly flag prevents XSS attacks which would exploit the easy way to access cookie data.
        | MaxAge String -- ^ The period in which the cookie will be send with the request.
        | Domain String -- ^ __Warning: Domain on localhost is forbidden and creates invalid cookies (<https://stackoverflow.com/questions/1134290/cookies-on-localhost-with-explicit-domain see>). __
        | Secure Bool -- ^ The secure flag restricts sending the cookie if the host is not secure. 
        deriving (Show,Eq)
   

    -- | Creates a cookie header out of a string.
    createCookieHeaderAttribute :: [Char] -> CookieHeaderAttribute
    createCookieHeaderAttribute input =
        let
            token = (splitOn "=" input)
            key = map toLower (head token)
            value = head (tail token)

        in case key of
            "path"      -> (Path value) 
            "expires"   -> (Expires value)
            "httponly"  -> (HttpOnly True)
            "max-age"   -> (MaxAge value)
            "domain"    -> (Domain value)
            "secure"    -> (Secure True)
    
    
    -- | Creates a cookie out of the http header attribute for cookies.
    cookieFactory :: String -> Cookie
    cookieFactory line = let fragments = splitOn "; " line
        in
            let keyValuePair = splitOn "=" (head fragments)
            in
                Cookie {
                    cookieName = head keyValuePair,
                    cookieValue = last keyValuePair,
                    cookieHeader = map createCookieHeaderAttribute (tail fragments)
                }


    -- | Transforms a CookieHeaderAttribute to a well formed cookie http header attribute.
    showCookieHeaderAttribute :: CookieHeaderAttribute -> String
    showCookieHeaderAttribute (Path path)      = "Path=" ++ path ++ "; "
    showCookieHeaderAttribute (Expires value)  = "Expires=" ++ value ++ "; "
    showCookieHeaderAttribute (HttpOnly value) = if value then "HTTPOnly; " else ""
    showCookieHeaderAttribute (MaxAge value)   = "Max-Age=" ++ value ++ "; "
    showCookieHeaderAttribute (Domain value)   = "Domain=" ++ value ++ "; "
    showCookieHeaderAttribute (Secure value)   = if value then "Secure; " else ""


    -- | Extracts the value of a cookie attribute.
    cookieAttribute :: String 
        -> Cookie 
        -> Maybe String
    cookieAttribute key cookie = extractValue 
        $ find(attributePredicate key) 
        $ cookieHeader cookie
        where
            attributePredicate :: String 
                -> CookieHeaderAttribute 
                -> Bool
            attributePredicate "Path" (Path a)          = True
            attributePredicate "Expires" (Expires a)    = True
            attributePredicate "HttpOnly" (HttpOnly a)  = True
            attributePredicate "Max-Age" (MaxAge a)     = True
            attributePredicate "Domain" (Domain a)      = True
            attributePredicate "Secure" (Secure a)      = True
            attributePredicate _ _                      = False

            extractValue :: Maybe CookieHeaderAttribute -> Maybe String
            extractValue (Just (Path a))      = Just a
            extractValue (Just (Expires a))   = Just a
            extractValue (Just (HttpOnly a))  = Just $ show a
            extractValue (Just (MaxAge a))    = Just a
            extractValue (Just (Domain a))    = Just a
            extractValue (Just (Secure a))    = Just $ show a
            extractValue Nothing              = Nothing


    -- | Folds the list of CookieHeaderAttributes and returns a concated list of these attributes.
    cookieHeaderBuilder :: [CookieHeaderAttribute] -> String
    cookieHeaderBuilder cookieHeaderAttributes = foldl (\x y -> x ++ showCookieHeaderAttribute y)
        [] cookieHeaderAttributes


    -- | Creates a well formed http cookie attribute, that is used for building the http response.
    cookieToString :: Cookie -> String
    cookieToString cookie = cookieName cookie ++ "="
        ++ cookieValue cookie ++ "; "
        ++ cookieHeaderBuilder (cookieHeader cookie)
