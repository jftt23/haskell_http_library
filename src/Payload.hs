{-|
Module      : Payload
Description : Handling different content-types of http bodies
License     : BSD 3-clause aka New BSD
Maintainer  : joshua.fett@mni.thm.de
Stability   : Draft

Right now no one is working on this module, as we decided that it is up to the user
how he/she handles content. 
-}

module Payload where

    data Payload = JSON String 
        | XML String 
        | HTML String 
        | PlainText String
        | File String deriving Show

    payloadToString :: Payload -> String
    payloadToString content = "Hier kommt Inhalt raus"
