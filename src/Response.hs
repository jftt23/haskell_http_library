{-|
Module      : Response
Description : Transforms response instances to a valid http response.
License     : BSD 3-clause aka New BSD
Maintainer  : joshua.fett@mni.thm.de
Stability   : Draft 

This module deals with http responses. It transforms a Response to a valid http response.
-}
module Response where
    import Cookie
    import Data.List
    import Data.List.Split
    import Data.Maybe
        
    type Body = String -- ^ represents a http body
    type StatusCode = Int -- ^ represents a http status code


    -- | This is the data type equivalent of a http response.    
    data Response = Response {
        responseHeader   :: [ResponseHeaderAttribute],  -- ^ the property responseHeader stores a list of "ResponseHeaderAttribute"s 
        responseBody     :: Maybe Body                  -- ^ the property reponseBody stores the Body as a Maybe monad
    } deriving Show

    
    -- | This data type represents a http response header attribute
    data ResponseHeaderAttribute = 
        StatusLine StatusCode String -- ^ The StatusLine containing the status code and the status response.
        | Connection String -- ^ Describes the connection.
        | ContentEncoding String -- ^ Defines the content encoding of the response.
        | ContentType String -- ^ The mime type of the response body.
        | Date String -- ^ The date of the outgoind response.
        | KeepAlive String -- ^ A tag for continous message transfer.
        | LastModified String -- ^ A date tag important for caching.
        | Server String -- ^ The host platform of the server.
        | SetCookie Cookie -- ^ The header attribute for setting cookies.
        | AccessControlAllowOrigin String -- ^ CORS Attribute.
        | TransferEncoding String -- ^ The form of encoding used to safely transfer the entity.
        | ResponseExpires String -- ^ Gives the date/time after which the response is considered stale.
        deriving (Show,Eq)

    
    -- | showHeaderAttribute transforms a ResponseHeaderAttribute to a valid HTTP Header Parameter 
    showHeaderAttribute :: ResponseHeaderAttribute -> [Char] 
    showHeaderAttribute (StatusLine code message)           = "HTTP/1.1 " ++ show code ++ " " ++ message
    showHeaderAttribute (Connection value)                  = "Connection: " ++ value
    showHeaderAttribute (ContentEncoding value)             = "Content-Encoding: " ++ value
    showHeaderAttribute (ContentType value)                 = "Content-Type: " ++ value
    showHeaderAttribute (Date value)                        = "Date: " ++ value
    showHeaderAttribute (KeepAlive value)                   = "Keep-Alive: " ++ value
    showHeaderAttribute (LastModified value)                = "Last-Modified: " ++ value
    showHeaderAttribute (Server value)                      = "Server: " ++ value    
    showHeaderAttribute (SetCookie cookie)                  = "Set-Cookie: " ++ cookieToString cookie
    showHeaderAttribute (AccessControlAllowOrigin value)    = "Access-Control-Allow-Origin: " ++ value 
    showHeaderAttribute (TransferEncoding value)            = "Transfer-Encoding: " ++ value
    showHeaderAttribute (ResponseExpires value)             = "Expires: " ++ value
    
    
    -- | Creates a HTTP Header out of a list of ResponseHeaderAttributes.
    responseHeaderBuilder :: [ResponseHeaderAttribute] -> [Char]
    responseHeaderBuilder responseHeaderAttributes = foldl (\x y -> x ++ Response.showHeaderAttribute y ++ "\n") 
        [] responseHeaderAttributes


    -- | Turns a response body to a http body.
    responseBodyBuilder :: Maybe Body -> [Char]
    responseBodyBuilder body = if (isJust body) 
        then fromJust body
        else ""

    
    -- | Transforms a Response to a well formed http response.
    responseToString :: Response -> [Char]
    responseToString response = responseHeaderBuilder (responseHeader response)
        ++ "\n"
        ++ responseBodyBuilder (responseBody response)
