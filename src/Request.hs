{-|
Module      : Request
Description : Transforms raw http request to request types.
License     : BSD 3-clause aka New BSD
Maintainer  : joshua.fett@mni.thm.de
Stability   : Draft

Transforms a valid http request into a Request. 
-}
module Request where
    import Cookie
    import Data.List.Split
    import Data.List
    import Data.Char
    import Data.Maybe
    import Numeric


    -- | This is the data type equivalent of a http request.
    data Request = Request {
        requestHeader   :: [RequestHeaderAttribute], -- ^ A list of RequestHeaderAttributes. This list can contain redundant header attributes.
        requestBody     :: Maybe String, -- ^ The request's body. Requests like GET or DELETE do not have a body which is why its of the type Maybe String.
        requestVar      :: Maybe String -- ^ The URL parameter which is for the RESTful way of accessing ressources. Used by GET, PUT, PATCH and DELETE.
    } deriving Show


    data RequestHeaderAttribute = 
        RequestTarget (String,String,String) -- ^ The request target containing the method, the path and the protocol version.
        | Host String -- ^ The domain name of the server with the port it is listening on.
        | UserAgent String -- ^ The user agent. 
        | RequestContentType String -- ^ The media type of the request body.
        | Accept String -- ^ Media types that are allowed for the response.
        | AcceptCharset String -- ^ Sets the allowed character set of the response body.
        | AcceptEncoding String -- ^ Sets the allowed encoding of the response body.
        | ContentLength String -- ^ Defines the length of the request body.
        | RequestCookie Cookie -- ^ The cookie header attribute for requests. 
        | InvalidHeader (String,String) -- ^ This header catches every undefined header attribute not implemented yet.
        deriving (Show,Eq)


    -- | Transforms a url encoded string (like an url) to a decoded string. 
    -- The url encoded character is made of a percent symbol and the hex value of the character's spot in the ascii table.
    -- The decoded character is replaced by the character of its ascii table spot.
    urlDecode :: String -> String
    urlDecode input = 
        let 
            splitted = splitOn "%" input
            token = tail splitted
        in 
            head splitted ++ (concat 
                $ map (\x -> (toEnum 
                    $ fst 
                    $ head 
                    $ readHex[head x, head (tail x)]::Char): tail (tail x)
                ) token
            )
        

    -- | Takes a String and creates a RequestHeaderAttribute.
    createHeaderAttribute :: [Char] -> RequestHeaderAttribute
    createHeaderAttribute input =
        let
            token = (splitOn " " input)
            key = (map toUpper (head token))
            fstParam = head (tail token)
            sndParam = last (tail token)

        in case key of
            "GET"               -> (RequestTarget ("GET",urlDecode fstParam,sndParam))
            "POST"              -> (RequestTarget ("POST",urlDecode fstParam,sndParam))
            "PUT"               -> (RequestTarget ("PUT",urlDecode fstParam,sndParam))
            "PATCH"             -> (RequestTarget ("PATCH",urlDecode fstParam,sndParam))
            "DELETE"            -> (RequestTarget ("DELETE",urlDecode fstParam,sndParam))
            "OPTIONS"           -> (RequestTarget ("OPTIONS",urlDecode fstParam,sndParam))
            "HEAD"              -> (RequestTarget ("HEAD",urlDecode fstParam,sndParam))
            "HOST:"             -> (Host fstParam)
            "USER-AGENT:"       -> (UserAgent fstParam)
            "CONTENT-TYPE:"     -> (RequestContentType fstParam)
            "ACCEPT:"           -> (Accept fstParam)
            "ACCEPT-CHARSET:"   -> (AcceptCharset fstParam)
            "ACCEPT-ENCODING:"  -> (AcceptEncoding fstParam)
            "CONTENT-LENGTH:"   -> (ContentLength fstParam)
            "COOKIE:"           -> (RequestCookie $ cookieFactory fstParam)
            _                   -> (InvalidHeader (key,fstParam))
            

    -- | Takes multiple strings and creates a list of RequestHeaderAttribute.
    createHeaderAttributes :: [[Char]] -> [RequestHeaderAttribute]
    createHeaderAttributes input = map createHeaderAttribute input   
    
    
    -- | Injects the request parameter of the url in the Request data.
    createRequestVar :: String 
        -> Request 
        -> Request
    createRequestVar var req = req {requestVar = Just var}

    
    -- | Takes the http request as string and creates a request.
    createRequest :: [Char] -> Request
    createRequest input = 
        let 
            rawRequest = splitOn "\r\n\r\n" input 
            rawHeader = splitOn "\r\n" $ head rawRequest
            requestHeaderAttributes = createHeaderAttributes rawHeader
            maybeContentLength = Request.extractValue 
                $ find(Request.attributePredicate "Content-Length") requestHeaderAttributes
        
            contentLength = if(isJust maybeContentLength)
                then read 
                    $ fromJust maybeContentLength :: Int 
                else 0
                
            rawBody = fst 
                $ splitAt contentLength 
                $ head (tail rawRequest)
                
            body = if(length rawBody > 0) then Just rawBody else Nothing
        
        in Request {
            requestHeader = requestHeaderAttributes,
            requestBody = body,
            requestVar = Nothing
        }


    -- | Takes the RequestHeaderAttribtues and returns the RequestTarget value as a tuple.
    getRequestTarget :: [RequestHeaderAttribute] -> (String,String,String)
    getRequestTarget ((RequestTarget x):xs) = x
    getRequestTarget (x:xs) = getRequestTarget xs
    getRequestTarget [] = ("","","")
    
    
    -- | Extracts the url parameter of the Request and returns it.
    getRequestVar :: Request -> String
    getRequestVar req = fromJust (requestVar req)

    
    -- | Helper functions for extracting the first element of tuple with three elements.
    fst' :: (a,b,c) -> a
    fst' (x,_,_) = x
    
    
    -- | Helper functions for extracting the second element of tuple with three elements.
    snd' :: (a,b,c) -> b
    snd' (_,x,_) = x
    
    -- | Helper function for switching the second element of tuple with three elements
    cPath :: b 
        -> (a,b,c) 
        -> (a,b,c)
    cPath b (a,_,c) = (a,b,c)

    -- | Helper function for finding certain attributes within the requestHeader.
    attributePredicate :: String 
        -> RequestHeaderAttribute 
        -> Bool
    attributePredicate "Host" (Host a)                          = True
    attributePredicate "User-Agent" (UserAgent a)               = True
    attributePredicate "Content-Type" (RequestContentType a)    = True
    attributePredicate "Accept" (Accept a)                      = True
    attributePredicate "Accept-Charset" (AcceptCharset a)       = True
    attributePredicate "Accept-Encoding" (AcceptCharset a)      = True
    attributePredicate "Content-Length" (ContentLength a)       = True
    attributePredicate "Request-Target" (RequestTarget a)       = True
    attributePredicate "Cookie" (RequestCookie a)               = True
    attributePredicate _ _                                      = False
    
    
    -- | Helper function for extracting the value of a RequestHeaderAttribute.
    extractValue :: Maybe RequestHeaderAttribute -> Maybe String
    extractValue (Just (Host a))                = Just a
    extractValue (Just (UserAgent a))           = Just a
    extractValue (Just (RequestContentType a))  = Just a
    extractValue (Just (Accept a))              = Just a
    extractValue (Just (AcceptCharset a))       = Just a
    extractValue (Just (AcceptEncoding a))      = Just a
    extractValue (Just (ContentLength a))       = Just a
    extractValue Nothing                        = Nothing

    -- | Helper function for extracting the cookie of a RequestHeaderAttribute.
    extractCookie :: Maybe RequestHeaderAttribute -> Maybe Cookie
    extractCookie (Just (RequestCookie a)) = Just a
    extractCookie _ = Nothing
    
    
    -- | Extract cookie of the request.
    getCookie :: String 
        -> Request 
        -> Maybe Cookie
    getCookie key req = find(\c -> cookieName c == key)
        $ extractCookie
        $ find(\x -> Request.attributePredicate "Cookie" x) 
        $ requestHeader req
    
    
    -- | Extract a certain header attribute of the request.
    get :: String 
        -> Request 
        -> Maybe String
    get key req = Request.extractValue 
        $ find(Request.attributePredicate key) 
        $ requestHeader req
